const { InvalidInputsError } = require('../errors')


function authCheck(req, res, next) {
    let params = { ...(req.body), ...(req.query)}
    let fields = ['username', 'password']

    let err_data = []

    fields.forEach( field => {
        if (typeof params[field] != 'string')
            err_data.push({ field, type: 'string' })
    })

    if (err_data.length > 0)
        InvalidInputsError(res, err_data)
    else
        next()
}


function calculatorCheck(req, res, next) {
    let params = { ...(req.body), ...(req.query)}
    let fields = ['x', 'y']

    let err_data = []

    fields.forEach( field => {
        if (isNaN(params[field]))
            err_data.push({ field, type: 'number' })
        else
            req.body[field] = Number(params[field])
    })

    if (err_data.length > 0)
        InvalidInputsError(res, err_data)
    else
        next()
}

module.exports = {
    authCheck,
    calculatorCheck
}
