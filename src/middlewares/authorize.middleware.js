const jwt = require('jsonwebtoken')

const { MissingTokenError, InvalidTokenError } = require('../errors')
const { JWT_SECRET } = require('../secrets')

module.exports = function (req, res, next) {
    let auth = req.headers['authorization']

    if (auth && auth.startsWith("Bearer ")) {
        let token = auth.slice(7)

        try {
            req.body.user = jwt.verify(token, JWT_SECRET)
            next()
        }
        catch (err) {
            InvalidTokenError(res, token)
        }
    }
    else
        MissingTokenError(res)
}
