const express = require('express')
const authorize = require('../middlewares/authorize.middleware')
const { calculatorCheck } = require('../middlewares/typeCheck.middleware')

const router = express.Router()

router.use(authorize)
router.use(calculatorCheck)

router.post('/sum', function(req, res) {
    let { x, y } = req.body

    res.send({ result: x + y })
})

module.exports = router
