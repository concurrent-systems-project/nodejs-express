const express = require('express')
const jwt = require('jsonwebtoken')

const { InvalidPasswordError } = require('../errors')
const { authCheck } = require('../middlewares/typeCheck.middleware')
const { JWT_SECRET, PASSWORD } = require('../secrets')

const router = express.Router()

router.use(authCheck)

router.post('/login', function(req, res) {
    let { username, password } = req.body

    if (password == PASSWORD)
        res.send({ token: jwt.sign({ username }, JWT_SECRET, { expiresIn: '5m' }) })
    else
        InvalidPasswordError(res, password, PASSWORD)
})

module.exports = router
