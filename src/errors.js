function throwError(res, message, type, code, data) {
    let err = {
        message,
        type,
        code,
        data
    }

    res.status(code).send(err)
}

module.exports = {
    MissingTokenError: (res) => {
        throwError(
            res,
            message = "Request header 'Authorization' not valid.",
            type = 'MISSING_TOKEN',
            code = 401,
            data = {
                message: "You must specify your token in the 'Authorization' request's header.",
                headers_example: {'Content-Type': 'application/json', 'Authorization': 'Bearer TOKEN'}
            }
        )
    },

    InvalidTokenError: (res, token) => {
        throwError(
            res,
            message = "Invalid token, please login again to receive a new one.",
            type = "INVALID_TOKEN",
            code = 401,
            data = { invalid_token: token }
        )
    },

    InvalidPasswordError: (res, password, password_valid) => {
        throwError(
            res,
            message = "Invalid password, please try again.",
            type = 'INVALID_PASSWORD',
            code = 417,
            data = { message: `You have entered '${password}'`, password: password_valid}
        )
    },

    InvalidInputsError: (res, data) => {
        throwError(
            res,
            message = "Invalid inputs: it seems some inputs are missing are invalid.",
            type = 'INVALID_INPUTS',
            code = 422,
            data
        )
    }
}
