const express = require('express')
const bodyParser = require('body-parser')

const authRouter = require('./src/services/auth.service')
const calculatorRouter = require('./src/services/calculator.service')

let app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/auth', authRouter)
app.use('/calculator', calculatorRouter)

app.listen(8000, () => {
    console.log("Server started")
})
