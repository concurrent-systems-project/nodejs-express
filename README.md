![Express logo](https://scotch-res.cloudinary.com/image/upload/w_1000,q_auto:good,f_auto/media/https://scotch.io/wp-content/uploads/2014/11/node-express-sendfile.png)

# Node.js - Moleculer

The goal of this project is to set a [Node.js](https://nodejs.org/en/) example of **microservices** using the [Express](https://expressjs.com/) framework.
For that purpose we create a **calculator** using an authentification system.

Here is a list of the npm packages used:
- [express](https://github.com/expressjs/express)
- [body-parser](https://github.com/expressjs/body-parser)
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) (JWT)


# Features

- HTTP requests
- Authentification login via common password
- Authorization via JSON Web Tokens valid for 5 min
- Calculator operations (only sum)

# HTTP Requests

The following HTTP requests are based on the **localhost** IP Adresse: **http://localhost:8000**

## Public

### POST /public/login

Login request where you specify a username and the password.
The authentification system is not link to a database, so the username can be random and the password is common for everyone.

Returns a JSON Web Token if the password is correct.

| Parameter   | Type     | Description                      |
| :---------: | :------: | -------------------------------- |
| `username`  | `String` | Any username would work          |
| `password`  | `String` | Common password: 'microservices' |

---

## Calculator

### POST /calculator/sum

Sum request between two numbers.
You need to give a valid token in the 'Authorization' header.

Returns the result of (x + y).

| Parameter   | Type     | Description          |
| :---------: | :------: | -------------------- |
| `x`         | `Number` | Any integer or float |
| `y`         | `Number` | Any integer or float |
